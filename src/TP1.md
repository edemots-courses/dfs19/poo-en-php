# TP1 - Un nouveau pouvoir

## But du TP

Ajouter un nouveau personnage qui va pouvoir, en plus de se battre, soigner d'autres personnages.

## Etape 1

Ecrire la méthode `soigne` dans la classe **Personnage**. Cette méthode aura pour but de rendre X points de vie au personnage reçu en paramètre.

> 💡 HINT
>
> Utilisez l'injection de dépendances

---

## Etape 2

Ajoutez un nouveau personnage au jeu nommé "**Francis**". Faites en suite en sorte qu'**Paul** batte **Jacques**.

> ℹ️ INFO
>
> A vous de choisir un bon montant de points de vie à rendre pour que Paul survive !

---

## Résultat attendu

```
Paul inflige 30 de dégats à Jacques !
Jacques reste haut en points de vie.

Jacques inflige 40 de dégats à Paul !
Il ne reste que 10 points de vie à Paul !

Francis rend X points de vie à Paul !

Paul inflige 30 de dégats à Jacques !
Jacques reste haut en points de vie.

Jacques inflige 40 de dégats à Paul !
Il ne reste que X points de vie à Paul !

Francis rend X points de vie à Paul !

Paul inflige 30 de dégats à Jacques !
Jacques a succombé sous les coups d'Paul.
```