# TP2 - Séparation of concerns

## But du TP

Séparer nos différents personnages selon de classes définies avec des comportements différents

Cette séparation devra comprendre l'utilisation :
- de l'héritage
- d'interface.s
- de constantes
- des modificateurs `abstract` et `final`
- "What else ?"

> 💡 HINT
>
> - On ne doit pas pouvoir instancier de Character tel quel.
> - Chaque personnage a des attributs constants (vie, dégats, ...).
> - Pensez à bien gérer les visibilités de vos attributs et méthodes.

## Résultat attendu

Le même qu'avant, mais avec de la POO sympa