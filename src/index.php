<?php

spl_autoload_register(function (string $classname) {
    $file = str_replace('\\', DIRECTORY_SEPARATOR, $classname).'.php';

    if (file_exists($file)) {
        require($file);
    }
});

use Classes\Wizard;

$paul = new Wizard("Paul");
$jacques = new Wizard("Jacques");
$francis = new Wizard("Francis");

// 1
$paul->hit($jacques);
$jacques->hit($paul);
$francis->heal($paul);
echo "<br/>";
// 2
$paul->hit($jacques);
$jacques->hit($paul);
$francis->heal($paul);
echo "<br/>";
// 3
$paul->hit($jacques);
$jacques->hit($paul);
$francis->heal($paul);
echo "<br/>";