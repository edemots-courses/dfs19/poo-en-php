<?php

namespace Classes;

final class Wizard extends Character
{
    const MAX_HEALTH = 50;
    const DAMAGES = 40;

    public function __construct(string $name)
    {
        $this->name = $name;
        $this->health = self::MAX_HEALTH;
    }

    public function hit(Character $character)
    {
        if (!$this->isDead()) {
            echo $this->name." foudroie ".$character->name." avec ".self::DAMAGES." de dégats <br />";
            // On appelle la méthode "hit" du parent
            parent::hit($character);
        }
    }
}