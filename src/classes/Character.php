<?php

namespace Classes;

use Interfaces\CanFight;

abstract class Character implements CanFight
{
    const DAMAGES = 0;

    protected $name;
    protected $health;
    public $healing;
    public $killedBy;

    public function __construct(string $name, int $health, ?int $healing = null)
    {
        $this->name = $name;
        $this->health = $health;
        $this->healing = $healing;
    }
    /**
    * Notation PHP8.0+ d'un constructeur
    * @link https://www.php.net/releases/8.0/en.php#constructor-property-promotion
    */
    // public function __construct(
    // public string $name,
    // public int $health,
    // public int $damages,
    // ) {
    // }

    /**
    * Permet de taper un autre personnage
    * Prend en paramètre une instance de Character (Injection de dépendances)
    */
    public function hit(Character $character)
    {
        // Le personnage passé en paramètre prends des dégats
        // On appelle getHit() sur l'instance du personnage passé en paramètre

        $character->getHit(static::DAMAGES, $this->name);
    }

    /**
    * Methode pour infliger des dégats à notre personnage
    */
    final public function getHit(int $damages, string $hitBy)
    {
        // Diminuer la vie du personnage en fonction des dégats
        $this->health -= $damages;
        $this->killedBy = $hitBy;
        echo $this->name.": ".$this->health."<br/>";
        echo $this;
    }

    /**
    * Permet de rendre de la vie à un personnage
    */
    public function heal(Character $character)
    {
        $character->health += $this->healing;
        echo $this->name . " rend " . $this->healing . " points de vie à " . $character->name . "<br />";
    }

    /**
    * Regarde si notre personnage est mort
    */
    public function isDead(): bool
    {
        return $this->health <= 0;
    }

    /**
    * Vérifie que notre personnage n'est pas sous un certain seuil
    */
    public function isUnder(int $life): string
    {
        if ($this->health <= $life) {
            return $this->name . " est en mauvaise posture." . "<br />";
        }

        return $this->name . " est haut en points de vie." . "<br />";
    }

    /**
    * Renvoie le status du personnage
    */
    public function status(): string
    {
        if ($this->isDead()) {
            return $this->name . " a succombé sous les coups d'" . $this->killedBy . "<br />";
        }

        return $this->isUnder(10);
    }

    /**
    * Est appelée lorsqu'on essaye d'afficher notre instance sous forme de string
    */
    public function __toString(): string
    {
        // Récupérer le status du personnage
        return $this->status();
    }
}