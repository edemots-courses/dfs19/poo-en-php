# PHP P.O.O.

## Prerequisites

*One or the other*

- [ ] [MAMP](https://www.mamp.info/en/downloads/) (MacOS), [WAMP](https://www.wampserver.com/en/download-wampserver-64bits/) (Windows) or LAMP
- [ ] [docker](https://docs.docker.com/get-docker/)

> ⚠️ For those working on Linux you'll need to install [docker-compose](https://docs.docker.com/compose/install/#install-compose-on-linux-systems) manually, on MacOS or Windows it's included with the desktop app.

## Installation

Clone the project

```shell
# if your using ssh
git clone git@gitlab.com:edemots-courses/dfs19/poo-en-php.git destination_folder
# with https
git clone https://gitlab.com/edemots-courses/dfs19/poo-en-php.git destination_folder
```

## Initialization

### With MAMP, WAMP or LAMP

Make sure to clone the project inside your `www/` directory or in your document root directory
(`/Users/your-name/Sites/localhost` for MAMP)

### With docker-compose

Just run in your terminal

```shell
docker-compose up
```

You can now access the application on **http://localhost**